export default class Character {
    constructor(name, weapon) {
        this.name = name;
        this.weapon = weapon;
        this.quest = "";
    }
}
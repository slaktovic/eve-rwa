import {Observable} from 'rxjs';

export default class BossFightService {

    constructor(characters){
        
        this.init(characters);

        //Event Listeners and binding
        let btnBossFight = document.getElementById('js-boss-btn');
        btnBossFight.addEventListener('click',this.startEncounter.bind(this));
        
        this.getTheBossEncounter = this.getTheBossEncounter.bind(this);
        this.fight = this.fight.bind(this);
        this.getLoot = this.getLoot.bind(this);
        this.renderWeaponSwap = this.renderWeaponSwap.bind(this);   
        this.renderWeaponCard = this.renderWeaponCard.bind(this);    
    }       

    init(characters) {
        this.characters = characters;
    }

    getTheBossEncounter() {
        const endPoint = "https://api.guildwars2.com/v2/raids/hall_of_chains";
        return fetch(endPoint)
                .then((res) => res.json())
                .catch((error) => console.log(error));
    }

    startEncounter() {
        const raid = Observable.fromPromise(this.getTheBossEncounter());
        
        const bossNum = Math.floor(Math.random() * 4);

        const bossHealth = Math.floor(Math.random() * 1000) + 2000;

        const boss  = { name: "", hp: bossHealth }

        raid.subscribe(data => {
            let name = data.wings[0].events[bossNum].id;
            name = name.replace(/_/g,' ');
            name = name[0].toUpperCase() + name.substr(1);
            boss.name = name;
            
            let btnQuest = document.getElementById('js-quest-btn');
            btnQuest.disabled = true;
            let btnBossFight = document.getElementById('js-boss-btn');
            btnBossFight.disabled = true;

            this.fight(boss);
        })
    }

    fight(boss) {
        let arenaSection = document.getElementById('js-arena');
        arenaSection.className = 'grid-container';
        let fightSection = document.createElement('section');
        fightSection.className = 'fight-section grid-container';
        let headline = document.createElement('h4');
        headline.className = 'grid-item';
        headline.innerHTML = `
            ${this.characters[0].name} and ${this.characters[1].name} are fighting 
            <strong>${boss.name}</strong>: ${boss.hp}hp!    
        `;
        
        arenaSection.appendChild(fightSection);
        fightSection.appendChild(headline);

        let firstCharDmgSpan = this.characters[0].weapon.details.max_power - this.characters[0].weapon.details.min_power;
        let secondCharDmgSpan = this.characters[1].weapon.details.max_power - this.characters[1].weapon.details.min_power;

        let strike = Math.floor(Math.random() * firstCharDmgSpan) + this.characters[0].weapon.details.min_power;
        let warrior = this.characters[0];
        let fightingIndex = 0;
    
        while(boss.hp > 0) {
            let fightLog = document.createElement('p');
            fightLog.innerHTML = `${warrior.name} dealt to ${boss.name} <strong>${strike}</strong> damage`;
            fightLog.className = "grid-item";
            fightSection.appendChild(fightLog);

            let remainingHealth = boss.hp -strike;
            boss.hp = remainingHealth > 0 ? remainingHealth : 0;

            let stats = document.createElement('p');
            stats.className = 'grid-item';
            stats.innerHTML = `Current Boss health: <strong>${boss.hp}</strong>`;

            fightSection.appendChild(stats);

            if(boss.hp == 0) {
                let finish = document.createElement('h5');
                finish.innerHTML = `${warrior.name} has killed ${boss.name} and will get loot!`;
                finish.className = 'grid-item';
                let btnLoot = document.createElement('button');
                btnLoot.className = 'game-btn';
                btnLoot.id = 'js-loot-btn';
                btnLoot.innerHTML = 'Get Loot!';

                fightSection.appendChild(finish);
                fightSection.appendChild(btnLoot);
                this.getLoot(warrior);
            }else {
                fightingIndex = fightingIndex == 0 ? 1 : 0;
                warrior = this.characters[fightingIndex];

                if(fightingIndex == 0) strike = Math.floor(Math.random() * firstCharDmgSpan) + warrior.weapon.details.min_power;
                else strike = Math.floor(Math.random() * secondCharDmgSpan) + warrior.weapon.details.min_power;  
            }
        }
    }

    getLoot(warrior){
        let btnLoot = document.getElementById('js-loot-btn');
        let click = Observable.fromEvent(btnLoot,'click');
        
        click.switchMap(click => {
            btnLoot.disabled = true;
            return Observable.fromPromise(this.getNewWeapon());
        })
        .subscribe(newWeapon => this.renderWeaponSwap(warrior,newWeapon));

    }

    getNewWeapon(){
        const weaponNum = Math.floor(Math.random()*1000) + 14001;
        const endPoint = "https://api.guildwars2.com/v2/items/" + weaponNum;
        return fetch(endPoint)
                    .then((res) => res.json())
                    .catch((error) => console.log(error));
    }

    renderWeaponSwap(character,newWeapon) {
        let arenaSection = document.getElementById('js-arena');
        arenaSection.className = 'grid-container';
        
        let headline = document.createElement('h4');
        headline.className = 'grid-item swap-headline';
        headline.innerHTML = "Winner's weapon is changed !";
        arenaSection.appendChild(headline);

        let lootSwapSection = document.createElement('section');
        lootSwapSection.className = 'loot-swap-section swap-grid-container';
        arenaSection.appendChild(lootSwapSection);
        

        let oldWeaponCard = document.createElement('div');
        oldWeaponCard.className = 'swap-card';
        oldWeaponCard.innerHTML = this.renderWeaponCard(character.weapon);

        let arrowCard = document.createElement('div');
        arrowCard.className = 'arrow-card';

        let newWeaponCard = document.createElement('div');
        newWeaponCard.className = 'swap-card';
        newWeaponCard.innerHTML = this.renderWeaponCard(newWeapon);

        lootSwapSection.appendChild(oldWeaponCard);
        lootSwapSection.appendChild(arrowCard);
        lootSwapSection.appendChild(newWeaponCard);

        let btnReset = document.createElement('button');
        btnReset.className = 'game-btn';
        btnReset.innerHTML = 'Reset the game';
        btnReset.addEventListener('click', this.resetTheGame.bind(this));

        arenaSection.appendChild(btnReset);
    }

    renderWeaponCard(weapon) {
        return `
        <div class="card" style="width:300px">
            <img class="card-img-top" src="${weapon.icon}">
            <div class="card-body">
                <p class="card-text"><strong>${weapon.name}</strong></p>
                <p class="card-text">Weapon type: ${weapon.details.type}</p>
                <p class="card-text">Min-power: ${weapon.details.min_power}</p>
                <p class="card-text">Max-power: ${weapon.details.max_power}</p>
            </div>
        </div>
        `
    } 

    resetTheGame(){
        window.location.reload();
    }
};
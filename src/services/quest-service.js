
export default class QuestService {

    constructor(characters){
        
        this.init(characters);

        let btnQuest = document.getElementById('js-quest-btn');
        btnQuest.addEventListener('click',this.assignQuests.bind(this));
        this.renderQuestCompletion = this.renderQuestCompletion.bind(this);
        this.renderQuestAndBossButtons = this.renderQuestAndBossButtons.bind(this);
    }

    init(characters) {
        this.characters = characters;
    }

    assignQuests() {

        let questContainer = document.createElement('div');
        questContainer.className = 'card-grid-container';

        let arenaSection = document.getElementById('js-arena');
        arenaSection.appendChild(questContainer);

        this.characters.forEach(character => {

            const questNum = Math.floor(Math.random()*13) + 10;
            const endPoint = "https://api.guildwars2.com/v2/stories/" + questNum;

            fetch(endPoint)
            .then((res) => res.json())
            .then(quest => {
                this.renderQuestCompletion(character,quest,questContainer);
                this.renderQuestAndBossButtons();
            })
            .catch((error) => console.log(error));
        });
    }

    renderQuestCompletion(character,quest, questContainer) {
        
        const time = Math.floor(Math.random()*15) + 10;
        
        let questCard = document.createElement('div');
            questCard.className = 'quest-card';
            questCard.innerHTML = `
                <div class="card" style="width:300px">
                     <div class="card-body">
                        <h4 class="card-title">${quest.name}</h4>
                        <h5 class="card-title">${quest.description}</h5>
                        <p class="card-text"><strong>${character.name}</strong> completed the quest.</p>
                        <p class="card-text">Time needed: <strong>${time} minutes</strong></p>
                    </div>
                </div>
            `
        questContainer.appendChild(questCard);
    }

    renderQuestAndBossButtons() {
        let arena = document.getElementById('js-arena');
        let btnGridContainer = document.getElementById('js-quest-boss-container');
        let questBtn = document.getElementById('js-quest-btn');
        let bossBtn = document.getElementById('js-boss-btn');
                
        arena.removeChild(btnGridContainer);
        arena.appendChild(btnGridContainer);
        btnGridContainer.appendChild(questBtn);
        btnGridContainer.appendChild(bossBtn);
    }
};
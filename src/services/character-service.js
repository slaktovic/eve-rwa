import Character from '../models/character';

export default class CharacterService {

    constructor(){
        
        this.init();

        //Event Listeners and binding
        let btnCreateCharacter = document.getElementById('js-create-character-btn');
        btnCreateCharacter.addEventListener('click',this.createCharacter.bind(this));
        
        this.renderCharacter = this.renderCharacter.bind(this);
        this.restrictNewCharacters = this.restrictNewCharacters.bind(this);
    }       

    init() {
        this.characters = [];

        let charCreationContainer = document.createElement('div');
        charCreationContainer.id = 'js-character-creation';
        let charCreationSection = document.getElementsByClassName('character-creation');
        charCreationSection[0].appendChild(charCreationContainer);
    }   

    createCharacter() {

        let inputName = document.getElementById('js-create-character-input');
        if(inputName.value == '') return;

        const weaponNum = Math.floor(Math.random()*1000) + 14001;
        const endPoint = "https://api.guildwars2.com/v2/items/" + weaponNum;
    
        fetch(endPoint)
        .then((res) => res.json())
        .then(weapon => {

            const name = inputName.value;
            inputName.value = '';
            
            let character = new Character(name,weapon);

            this.renderCharacter(character);

            this.characters.push(character);
            
            this.restrictNewCharacters();
        })
        .catch((error) => console.log(error));
    }

    renderCharacter(character){
        let weapon = character.weapon;

        let profile = document.createElement('div');
            profile.className = 'character-profile';
            profile.innerHTML = `
                <div class="card">
                    <img class="card-img-top" src="${weapon.icon}">
                     <div class="card-body">
                        <h4 class="card-title"><strong>${character.name}</strong></h4>
                        <p class="card-text">Weapon name:</p>
                        <p class="card-text"><strong>${weapon.name}</strong></p>
                        <p class="card-text">Weapon type: ${weapon.details.type}</p>
                        <p class="card-text">Min-power: ${weapon.details.min_power}</p>
                        <p class="card-text">Max-power: ${weapon.details.max_power}</p>
                    </div>
                </div>
            `
        let charGridContainer = document.getElementById('js-character-creation');
        charGridContainer.className = 'card-grid-container';
        charGridContainer.appendChild(profile);
    }

    restrictNewCharacters(){
        if(this.characters.length == 2){
            let btnCreateCharacter = document.getElementById('js-create-character-btn');
            btnCreateCharacter.disabled = true;
            let headline = document.getElementById('js-headline');
            headline.innerHTML = "Characters created. Fight!";  
            
            let questBtn = document.getElementById('js-quest-btn');
            questBtn.disabled = false;
            let bossBtn = document.getElementById('js-boss-btn');
            bossBtn.disabled = false;

            let inputName = document.getElementById('js-create-character-input');
            inputName.value = '';
            inputName.disabled = true;
        }
    }
};
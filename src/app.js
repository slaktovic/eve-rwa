import CharacterService from './services/character-service';
import QuestService from './services/quest-service';
import BossFightService from './services/boss-fight-service';

let characterService = new CharacterService();
let questService = new QuestService(characterService.characters);
let bossFightService = new BossFightService(characterService.characters);

console.log("app is working");